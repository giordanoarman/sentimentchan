Package.describe({
  summary: 'Homepage',
});

Package.onUse(function (api) {
  api.versionsFrom('1.3');

  api.use(['ecmascript'], 'client');

  api.mainModule('Homepage.js', 'client');

  api.export('Homepage');
});

Npm.depends({
  react: '15.1.0',
});
