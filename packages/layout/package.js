Package.describe({
  summary: 'Layout',
});

Package.onUse(function (api) {
  api.versionsFrom('1.3');
  const both = [ 'client', 'server' ];

  api.use(['ecmascript'], both);

  api.mainModule('Layout.js', 'client');

  api.export('Layout');
});

Npm.depends({
  react: '15.1.0',
  'react-tap-event-plugin': '1.0.0',
});
