import React, { PropTypes } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

export default Layout = ({ content }) => {
  return <div className="layout">
    {content()}
  </div>;
}

Layout.propTypes = {
  content: PropTypes.func.isRequired,
};
