import React from 'react';

export default NotFound = () => {
  return (
    <div>
      <h2>404 - Page not found!</h2>
    </div>
  );
};
