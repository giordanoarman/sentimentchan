Package.describe({
  summary: 'Not found screen.',
});

Package.onUse(function (api) {
  api.versionsFrom('1.3');
  const both = ['client', 'server'];

  api.use('ecmascript', both);

  api.mainModule('NotFound.js', 'client');

  api.export(['NotFound']);
});

Npm.depends({
  react: '15.1.0',
});
