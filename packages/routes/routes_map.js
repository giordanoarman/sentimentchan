import { FlowRouter } from 'meteor/kadira:flow-router';
import { Homepage } from 'meteor/homepage';
import { Layout } from 'meteor/layout';
import { mount } from 'react-mounter';
import { NotFound } from 'meteor/not-found';
import React from 'react';

FlowRouter.notFound = {
  action() {
    mount(Layout, { content: () => <NotFound /> });
  }
};

FlowRouter.route('/', {
  name: 'home',
  action() {
    mount(Layout, { content: () => <Homepage /> });
  },
});
