Package.describe({
  summary: 'Routing and controllers.',
});

Package.onUse(function (api) {
  api.versionsFrom('1.3');
  const both = [ 'client', 'server' ];

  api.use([
    'ecmascript',
    'kadira:flow-router',
    'underscore',
    'layout',
  ], both);
  
  api.mainModule('routes.js', both);
  api.mainModule('routes_map.js', both);

  api.export('Routes');
});

Npm.depends({
  'react-mounter': '1.2.0',
  react: '15.1.0',
  'react-dom': '15.1.0',
});
